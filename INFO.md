# [ S  I  A  U  A ]()
#### *Credenciales*
    Cuenta
    hector_hdez_96@hotmail.com
    pass; SIAUAsistema.2022
    
    web_name: siaua22
    pass: SIAUA.2022
    
    
    pass_db: SIAUAsistema.2022
    user_db: id19351006_agua
    name:db: id19351006_siauadb
    
    Cuenta de gmail
    dev.sistema.agua@gmail.com
    prj.22.agua
    SIAUAsistema.2022
#### Es un sistema integral de Atención para Usuarios de Agua Potable.
    Funcionalidades en cada sección / modulo del sistema.

## **I. Inicio**
##### En este modulo encontraras información e imagenes referente al pozo de agua de la comunidad.

## **II. Servicios**
##### Funciones que pude realizar el Administrador:

    1. Filtrado de las personas por la Manzana que seleccione. 
    2. Registro de usuarios y su servicio de agua correspondiente.
    3. Registrar solamente el servicio a un Usuario ya Existente. 
    4. Cambio servicios.
    5. Dar de baja Servicios.
    6. Dar de baja usuarios.
    7. Edición de de los datos del servicio.
    8. Edición de los datos de la persona. 
    9. Buscar el registro  de personas por la Manzana, Nombre de la persona, Apellido paterno y materno.
    10. Registrar pagos.
    11. Dar de Alta nuevas tarjetas de cobro. 
    12. Vizualizar información del usuario, del o de sus servicios a que tenia a su nombre, cuanto es lo que debe de cada servicio con el que cuente .
---
## **III Manzanas**
##### Manzanas | Cuadras | Barrios: Proporción de espacio en la cual se divide la comunidad

    1. Registro de nueva manzana.
    2. Baja de manzana(Unicamente si y solo si no tiene registros asociados a esta).
    3. Borrar manzana sin relaciones con persona y servicio.
    4. Editar datos de manzana.
    5. Generación de informes por manzana.
### **IV Ciclo**
##### Representa el año actual o el perio en el cual se relizarón ciertos pagos, se dierón de alta nuevos usuarios etc.

    1. Agregar nuevos ciclos
    2. Activar y desactivar ciclos
    3. Editar datos del ciclo
    4. Eliminar el ciclo unicamente cuando no se tiene referencia en algun otra tabla

### **V Gastos**
##### Se genera un historial de los gastos que se van realizando en el transcurso de un ciclo.
    1. Registro de los gastos realizados.
    2. Generación de puntos de control para realizar cortes de caja.
    3. Edición de registros de los gatos.

### **VI Reportes**
##### Generación de reportes
    1. Generación de reportes por ciclos.
### **VII Acceso** 
##### 
    1. Dar de Alta nuevos inicios de sesión.
    2. Dar de baja los accesos que hayan culminado su ciclo.
### **VIII Sesion**
##### Muestra los datos de la persona que ha iniciado sesión.
    1. Cierra la sesión activa
    1