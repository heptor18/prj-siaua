
        <div class="card card-outline card-outline card-primary">
            <div class="card-body table-responsive">
                <table id="tb_acceso" class="table table-sm table-striped table-bordered text-center">
                    <thead class="table-primary">
                        <tr>
                            <th> # </th>
                            <th> Nombre </th>
                            <th> Comite </th>
                            <th> Estatus </th>
                            <th> Periodo </th>
                            <th> Opciones </th>
                        </tr>
                    </thead>
                    <tbody class="table-primary table-light"></tbody>
                </table>
            </div>
        </div>


<div class="modal fade" id="modal-acceso" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content card card-primary card-outline shadow">
            <div class="modal-header">
                <h4 class="modal-title text-center justify-content-between"><i class="fas fa-users"></i> ACCESOS
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-acceso" action="#" method="POST">
                <div class="modal-body card-body">
                    <input id="txt_id_comite" name="txt_id_comite" type="hidden">
                    <div class="form-group">
                        <label for="txt_id_persona">Responsable:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <select name="txt_id_persona" id="txt_id_persona" class="form-control selectpicker"
                                data-live-search="true" required></select>
                        </div>
                    </div>
                    <div class="form-group d-flex mb-0">
                        <div class="form-group mr-2">
                            <label for="txt_desc_servicio">Servicio:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-file"></i></span>
                                </div>
                                <input type="text" id="txt_desc_servicio" name="txt_desc_servicio" class="form-control"
                                    placeholder="Descripción del servicio" required>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="txt_idmanzana">Manzana:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-shapes"></i></span>
                                </div>
                                <select name="txt_idmanzana" id="txt_idmanzana" class="form-control selectpicker"
                                    data-live-search="true" required="">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="txt_direccion">Dirección:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-road"></i></span>
                            </div>
                            <input type="text" id="txt_direccion" name="txt_direccion" class="form-control"
                                placeholder="Dirección" required>
                        </div>
                    </div>


                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="app/view/js/view_acceso.js"></script>