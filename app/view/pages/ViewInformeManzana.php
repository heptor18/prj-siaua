<div class="row">
    <div class="col-md-4">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="public/assets/img/img_cuadra.png"
                        alt="Manzana">
                </div>
                <input type="hidden" id="txt_idmanzana">
                <h3 id="txt_nombre_manzana" class="profile-username text-center">---</h3>

                <p id="txt_desc_manzana" class="text-muted text-center">---</p>

                <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                        <b>Servicios Activos: </b> <a id="txt_servicios_activos" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b>Servicios Activos: </b> <a id="txt_servicios_inactivos" class="float-right">---</a>
                    </li>
                    <li class="list-group-item">
                        <b> No personas </b> <a id="txt_total_personas" class="float-right">---</a>
                    </li>

                </ul>
                <a href="#" class="btn btn-primary btn-block"><b>Editar</b></a>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->


    </div>
    <!-- /.col -->
    <div class="col-md-8">
        <div class="card card-info">
            <div class="card-body">

                <table id="tb_tarjeta"  class="table table-sm table-striped table-bordered text-center">
                    <thead class="table-primary">
                        <tr>
                            <th> # </th>
                            <th> Mes </th>
                            <th> % Pagado </th>
                            <th> $ Monto </th>

                        </tr>
                    </thead>
                    <tbody class="table-primary table-light">
                        <tr>
                            <td>1</td>
                            <td>Enero</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Febrero</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Marzo</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Abril</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Mayo</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Junio</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Julio</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Agosto</td>
                            <td> 86 % </td>
                            <td> $220, 500 </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Septiembre</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Octubre</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Noviembre</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>Diciembre</td>
                            <td>---</td>
                            <td>---</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
<script type="text/javascript" src="app/view/js/view_manzana_info.js"></script>