<div class="card card-outline card-primary">
    <div class="card-body">
        <table id="tb_manzana" class="table table-sm table-striped table-bordered text-center">
            <thead class="table-primary">
                <tr>
                    <th> # </th>
                    <th> Nombre </th>
                    <th> N° de personas </th>
                    <th> Opciones </th>
                </tr>
            </thead>
            <tbody class="table-primary table-light"></tbody>
        </table>
    </div>
</div>


<!---Modal para crear manzana-->
<div class="modal fade" id="modal-manzana" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content card card-primary card-outline shadow">
            <div class="modal-header">
                <h4 class="modal-title font-weight-bold text-center justify-content-between">
                    <i class="fas fa-map"></i> Manzana
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-manzana" action="#" method="POST">
                <div class="modal-body card-body">
                    <input id="txt_idmanzana" name="txt_idmanzana" type="hidden">
                    <div class="form-group">
                        <label for="txt_nombre_manzana">Nombre manzana:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                            </div>
                            <input type="text" id="txt_nombre_manzana" name="txt_nombre_manzana" class="form-control"
                                placeholder="Ingrese nombre de la manzana" maxlength="30" minlength="5" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txt_descripcion_manzana">Descripción:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-signs"></i></i></span>
                            </div>
                            <input type="text" id="txt_descripcion_manzana" name="txt_descripcion_manzana"
                                class="form-control" placeholder="Descripción: " maxlength="50" minlength="5" required>
                        </div>
                    </div>



                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!---Final de modal de crear manzanas-->
<script type="text/javascript" src="app/view/js/view_manzana.js"></script>