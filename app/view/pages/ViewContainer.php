<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">

    <!-- Site wrapper
    class="control-sidebar-slide-open layout-navbar-fixed layout-fixed sidebar-mini-xs"
-->
    <div class="wrapper">
        <?php include(VIEW."include".DS."NavbarInclude.php");?>
        <?php include(VIEW."include".DS."SidebarInclude.php");?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 id="page-title"></h4>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">

                <div class="container-fluid" id="panel_container">

                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->