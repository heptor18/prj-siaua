
        <div class="card card-outline card-outline card-primary">
            <div class="card-body">
                <table id="tb_persona" class="table table-sm table-striped table-bordered text-center">
                    <thead class="table-primary">
                        <tr>
                            <th> # </th>
                            <th> Estatus </th>
                            <th> Nombre </th>
                            <th> Dirección </th>
                            <th> Manzana </th>
                            <th> Opciones </th>
                        </tr>
                    </thead>
                    <tbody class="table-primary table-light"></tbody>
                </table>
            </div>
        </div>


<div class="modal fade" id="modal-user" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content card card-primary card-outline shadow">
            <div class="modal-header">
                <h4 class="modal-title  justify-content-between"><i class="fas fa-users"></i> REGISTRO DE USUARIO
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-usuario" action="#" method="POST">
                <div class="modal-body card-body">
                    <input id="txt_idpersona" name="txt_idpersona" type="hidden">
                    <div class="form-group">
                        <label for="txt_user">Nombre:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" id="txt_user" name="txt_user" class="form-control"
                                placeholder="Ingrese nombre" required>
                        </div>
                    </div>

                    <div class="form-group d-flex mb-0">
                        <div class="form-group mr-2">
                            <label for="txt_apepat">Apellido paterno:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" id="txt_apepat" name="txt_apepat" class="form-control"
                                    placeholder="Apellido Paterno" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txt_apemat">Apellido materno:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" id="txt_apemat" name="txt_apemat" class="form-control"
                                    placeholder="Apellido Materno" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txt_direccion">Dirección:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-road"></i></span>
                            </div>
                            <input type="text" id="txt_direccion" name="txt_direccion" class="form-control"
                                placeholder="Dirección" required>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="form-group mr-2">
                            <label for="txt_idmanzana">Manzana:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-shapes"></i></span>
                                </div>
                                <select name="txt_idmanzana" id="txt_idmanzana" class="form-control selectpicker"
                                    data-live-search="true" required="">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txt_edocivil">Estado civil:</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <select name="txt_edocivil" id="txt_edocivil" class="form-control selectpicker"
                                    data-live-search="true" required></select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="app/view/js/view_usuario.js"></script>