
        <div class="card card-outline card-outline card-primary">
            <div class="card-body">
                <table id="tb_pago" class="table table-sm table-striped table-bordered text-center">
                    <thead class="table-primary">
                        <tr>
                            <th> # </th>
                            <th> Estatus </th>
                            <th> Nombre </th>
                            <th> Descripción </th>
                            <th> Alta </th>
                            <th> Modificación </th>
                            <th> Opciones </th>
                        </tr>
                    </thead>
                    <tbody class="table-primary table-light"></tbody>
                </table>
            </div>
        </div>



<!---Modal para crear manzana-->
<div class="modal fade" id="modal-pago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card card-navy">
                <div class="card-header">
                    <h2 class="card-title text-center">Registro || Manzana</h2>
                    <button class="close text-white" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="card-body">

                    <form id="form-manzana">
                        <input id="idmanzana" name="idmanzana" type="hidden">
                        <div class="form-group">
                            <label for="nombre_manzana">Nombres:</label>
                            <input id="nombre_manzana" name="nombre_manzana" type="text" class="form-control"
                                placeholder="Ingrese nombre" require="">
                        </div>
                        <div class="form-group">
                            <label for="desc">Descripción:</label>
                            <input id="desc" name="desc" type="text" class="form-control"
                                placeholder="Ingrese Descripción" require="">
                        </div>
                        <div class="form-group">
                            <label for="modificacion">Fecha-Modificación:</label>
                            <input id="modificacion" name="modificacion" type="date" class="form-control"
                                placeholder="DD-MM-AAAA" require="">
                        </div>
                        <div class="form-group">
                            <label for="estatus_manzana">Estatus:</label>
                            <input id="estatus_manzana" name="estatus_manzana" type="text" class="form-control"
                                placeholder="Estatus" require="">
                        </div>


                </div>
                <div class="card-footer-sm p-0 m-0">
                    <div class="modal-footer mb-0 justify-content-between">
                        <button class="btn bg-navy" type="submit">Guardar</button>
                        <button class="btn btn-outline-secondary float-right" type="button"
                            data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Final de modal de crear manzanas-->
<script type="text/javascript" src="app/view/js/view_pago.js"></script>