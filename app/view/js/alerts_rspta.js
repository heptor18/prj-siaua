var Toast;
Toast = Swal.mixin({
    toast: true,
    animation: true,
    position: 'center'

});
function alert_rspt_success(tipo){
    Toast.fire({
        icon: 'success',
        title: tipo,
        showConfirmButton: false,
        timer: 3000
    });
}

function alert_rspt_error(tipo){
    Toast.fire({
        icon: 'error',
        title: tipo,
        showConfirmButton: false,
        timer: 3000
    });
}
/**
 * Confirmación para la eliminación de posibles registros
 * @param {int} id Es el identificador del registro que se intenta eliminar
 * @param {String} url_controller Dirección del Controlador asociado con la vista
 * @param {String} param Parameto que buscara el controlador para realizar el borrado del registro
 */
function alert_question_delete(id, url_controller, param){
    console.info("Elimina con ID = "  + id);
    Toast.fire({
        title: '¿Está seguro que desea eliminar este registro?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#2d89f2',
        cancelButtonColor: '#9a9a9c',
        confirmButtonText: '¡Sí, bórralo!',
        cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                $.post(
                    url_controller.concat('DELETEID'),
                    {param : id},
                    function(rspta){
                        console.log(param);
                        console.log(id);
                        console.log(rspta);
                        if (rspta == "FAILD") {
                            alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
                        } else {
                            alert_rspt_success(rspta);
                        }
                    }
                );
            } 
        });
}