// Dirección a
var url_servicio = 'app/controller/ServicioController.php?ACTN=';
/**
 *  Función: initViewServicio
 *      Es la encargada de llenar la tabla de servicios
 *      al igual que los elementos SELECT de las entidades Manzana y Persona
 *      que se muestran dentro del modal
 * */
function initViewServicio(){
    tbServicio();
    select_persona();
    select_manzana();
    $("#form-servicio").on('submit', function(e){
        saveServicio(e);
    });
}
/**
 * Función: addServicio
 *  Se encarga de accionar el evento para que se muestre el modal de registro
 */
function addServicio() {
    $('#form-servicio')[0].reset();
    $('#modal-servicio').modal({
        show: true,
        backdrop: 'static'
    });
}

/**
 * Función: saveServicio
 * función que guarda ya sea un registro o la modificación de un registro ya existente
 */
function saveServicio(e){
    e.preventDefault();
    var formData = new FormData($("#form-servicio")[0]);
    $.ajax({
        url: url_servicio.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-servicio")[0].reset();
                alert_rspt_success(data);
                tbServicio();
                $('#modal-servicio').modal('hide');
            }
        }
    });
}
/**
 * Función: goServicioID
 * 
 */
function goServicioID(id) {

}

function goEditServicio(id) {
    $.post(url_servicio.concat('ROWID'),
        { 'txt_id_servicio': id }, function (resp) {
            d = JSON.parse(resp);
            $("#txt_id_servicio").val(id);
            $("#txt_desc_servicio").val(d.desc_servicio);
            $("#txt_idmanzana").val(d.id_manzana)
            $("#txt_idmanzana").selectpicker('refresh');
            $("#txt_direccion").val(d.direccion);
            $("#txt_id_persona").val(d.id_persona);
            $("#txt_id_persona").selectpicker('refresh');
            $('#modal-servicio').modal({
                show: true,
                backdrop: 'static'
            });
        });
}
var tabla_servicio;
function tbServicio() {
    tabla_servicio = $("#tb_servicio").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            url: url_servicio.concat('LIST'),
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            },
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[1, "desc"]],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }
    }).buttons().container().appendTo("#tb_servicio_wrapper .col-md-6");
}

