var url_perfil = 'app/controller/PersonaController.php?ACTN=';
/**
 * initViewPerfil
 * Carga la información de la persona seleccionada. 
 * @param int id Identificador de la persona a mostrar su información
 */
function initViewPerfil(id) {
    $.post(url_perfil.concat('ROWID'),
        { 'txt_idpersona': id }, function (rspta) {
            d = JSON.parse(rspta);
            $("#txt_idpersona").html(id);
            $("#txt_nombre").html(d.nombre_persona);
            $("#txt_direccion").html(d.direccion);
            $("#txt_ape_pat").html(d.ape_pat);
            $("#txt_ape_mat").html(d.ape_mat)
            $("#txt_manzana").html(d.nombre_manzana);
            $("#txt_edo_civil").html(d.estado_civil);
            $("#txt_fecha_pago").html(d.fecha_pago_anio);
        });
} 