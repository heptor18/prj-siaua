$("#frm-login").on("submit", function(ev){
    ev.preventDefault();
    validaUsuario();
});

function validaUsuario(){
    user = $("#txt_user").val();
    pass = $("#txt_pass").val();
    $.post("app/controller/LoginController.php?ACTN=INT",
        {
            "txt_user":user,
            "txt_pass":pass
        }, function(res){
            if(res !== "success"){ // # NO entrara al sistema y se mostrara al usuario.
                msgModal(res);
            } else{
                acceso();
            }
        }
    );
}