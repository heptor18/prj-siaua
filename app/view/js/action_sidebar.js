var url_view = 'app/view/pages/';
/*** 
 * ! Inicio
 *      se muestra el modulo de usuarios 
 * */ 
 $("#tab_home").on("click", function(){ 
    initViewInicio();
});
/*** 
 * ! USUARIO
 *      se muestra el modulo de usuarios 
 * */ 
$("#tab_user").on("click", function(){
    $.post(url_view.concat('ViewUsuario.php'), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Usuarios <button class='btn btn-sm btn-primary' onclick='addUser()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewPeople();
    });
});

/*** 
 * !SERVICIOS
 *      se muestra el modulo de servicios 
 * */ 
 $("#tab_service").on("click", function(){
    $.post(url_view.concat("ViewServicio.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Servicios: " + "<button class='btn btn-sm btn-primary' onclick='addServicio()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewServicio();
    });
});

/*** 
 * ! MANZANAS
 *     se muestra el modulo de manmzanas 
 * */ 
 $("#tab_manzana").on("click", function(){
    $.post(url_view.concat("ViewManzana.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Manzanas: " + "<button class='btn btn-sm btn-primary' onclick='addManzana()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewManzana();
    });
});


/***
 * ! PAGOS 
 *     se muestra el modulo de pagos 
 * */ 
 $("#tab_pago").on("click", function(){
    $.post(url_view.concat("ViewPago.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Pagos: " + "<button class='btn btn-sm btn-primary' onclick='addPago()'><i class='fa fa-plus-circle'></i> Agregar</button>");
    });
});

/***
 * ! GASTOS 
 *     se muestra el modulo de gatos 
 * */ 
 $("#tab_gasto").on("click", function(){
    $.post(url_view.concat("ViewGasto.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Gastos: " + "<button class='btn btn-sm btn-primary' onclick='openModalGasto()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewGasto();
    });
});
/***
 * ! ACCESOS 
 *     se muestra el modulo de acceos 
 * */ 
 $("#tab_accesos").on("click", function(){
    $.post(url_view.concat("ViewAcceso.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Accesos: " + "<button class='btn btn-sm btn-primary' onclick='openModalAcceso()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewAcceso();
    });
});

/***
 * ! REPORTES 
 *     se muestra el modulo de reportes 
 * */ 
 $("#tab_reporte").on("click", function(){
    $.post(url_view.concat("ViewReporte.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Reporte: " + "<button class='btn btn-sm btn-primary' onclick='addReporte()'><i class='fa fa-plus-circle'></i> Agregar</button>");
    });
});

/***
 * ! CICLOS 
 *     se muestra el modulo de ciclos 
 * */ 
 $("#tab_ciclo").on("click", function(){
    $.post(url_view.concat("ViewCiclos.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Ciclos: " + "<button class='btn btn-sm btn-primary' onclick='addCiclo()'><i class='fa fa-plus-circle'></i> Agregar</button>");
        initViewCiclo();
    });
});

/***
 * ! AGUA 
 *     se muestra el modulo de tomas de agua 
 * */ 
 $("#tab_agua").on("click", function(){
    $.post(url_view.concat("ViewAgua.php"), function(resp){
        $("#panel_container").html(resp);
        $("#page-title").html("Persona - Servicio: " + "<button class='btn btn-sm btn-primary mr-3' onclick='openModalServicio()'><i class='fa fa-plus-circle'></i> Agregar servicio</button>"+
                                                     "<button class='btn btn-sm btn-primary' onclick='openModalPersona()'><i class='fa fa-plus-circle'></i> Agregar persona</button>");
        initViewAgua();
    });
});
