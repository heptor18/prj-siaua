var url_manzana_info = 'app/controller/ManzanaController.php?ACTN=';

function getInformationDetailManzanaID(id){
    $.post(url_manzana_info.concat('DETAILS'), {'txt_idmanzana': id}, 
        function(data){
            d = JSON.parse(data);
            console.log(d);
            $("#txt_idmanzana").val(id);
            $("#txt_nombre_manzana").html(d.nombre_manzana);
            $("#txt_desc_manzana").html(d.des_manzana);
            $("#txt_total_personas").html(d.numero_personas);
            $("#txt_servicios_activos").html(d.numero_serv_activos);
            $("#txt_servicios_inactivos").html(d.numero_serv_inactivos);
        });
}