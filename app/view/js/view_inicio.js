function initViewInicio(){
    $.post("app/view/pages/ViewInicio.php", function(response){
        $("#panel_container").html(response);
        $("#page-title").html("");
        $(".content-header").addClass('hide');
    });

}

function onClickImage(img){
    var url_img = '<img src="public/assets/img/';
    switch(img){
        case 1:
            $("#gallery").html(url_img.concat('img_01.jpg').concat('" class="product-image justify-content-center" style="height: 550px;">'));
            break;
        case 2:
            $("#gallery").html(url_img.concat('img_02.jpg').concat('" class="product-image justify-content-center" style="height: 550px;" >'));
            break;
        case 3:
            $("#gallery").html(url_img.concat('img_03.jpg').concat('" class="product-image justify-content-center" style="height: 550px;" >'));
            break;
        case 4:
            $("#gallery").html(url_img.concat('img_04.jpg').concat('" class="product-image justify-content-center" style="height: 550px;" >'));
            break;
        case 5:
            $("#gallery").html(url_img.concat('img_05.jpg').concat('" class="product-image justify-content-center" style="height: 550px;" >'));
            break;
    }
}

initViewInicio();