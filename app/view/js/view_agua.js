
// Dirección de Controlador
var url_agua = 'app/controller/ServicioController.php?ACTN=';
function initViewAgua() {
    select_edo_civil();
    select_manzana();
    select_persona();
    $("#form-usuario").on('submit', function (e) {
        addPersona(e);
    });

    $("#form-servicio").on('submit', function(e){
        saveServicio(e);
    });
    
    $("#txt_idmanzana").change(function () {
        console.log("Occión: " + $(this).val() );
    });
    aplicaFiltro(3);
}

function goPersonaId(id) {
    $.post("app/view/pages/ViewPerfil.php", function (rspta) {
        $("#panel_container").html(rspta);
        $("#page-title").html("Perfil de usuario");

        initViewPerfil(id);
    });
}

var tb_agua;
function aplicaFiltro(item){
    $("#tb_agua").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true,
        "ajax": {
            url: url_agua.concat('LIST'),
            data: {"txt_idmanzana_s": item},
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            },
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "asc"]],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }
    });
    
}
var url_servicio = 'app/controller/ServicioController.php?ACTN=';
/**
 * Función: addServicio
 *  Se encarga de accionar el evento para que se muestre el modal de registro
 */
 function openModalServicio() {
    $('#form-servicio')[0].reset();
    $('#modal-servicio').modal({
        show: true,
        backdrop: 'static'
    });
}
/**
 * Función: saveServicio
 * función que guarda ya sea un registro o la modificación de un registro ya existente
 */
 function saveServicio(e){
    e.preventDefault();
    var formData = new FormData($("#form-servicio")[0]);
    $.ajax({
        url: url_servicio.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-servicio")[0].reset();
                alert_rspt_success(data);
                $('#modal-servicio').modal('hide');
            }
        }
    });
}
var url_user = 'app/controller/PersonaController.php?ACTN=';
/**
 * ! Función addUser
 *   Se encarga de limpiar los campos del formulario
 *   de registro, porteriormente se muestra el modal.
 */
 function openModalPersona() {

    cleanInputs();
    $('#modal-user').modal({
        show: true,
        backdrop: 'static'
    });
}

function cleanInputs() {
    $("#txt_idpersona").val("");
    $("#txt_user").val("");
    $("#txt_apepat").val("");
    $("#txt_apemat").val("");
    $("#txt_edocivil").val("")
    $("#txt_edocivil").selectpicker();
    $("#txt_idmanzana_u").val("");
    $("#txt_idmanzana_u").selectpicker();
    $("#txt_direccion").val("");
}

/**
 * ! *Función addPersona
 * Envia los datos y la petición de registro al controlador
 * @param object e Evento que se lanza.
 */
function addPersona(e) {
    e.preventDefault();
    var formData = new FormData($("#form-usuario")[0]);


    $.ajax({
        url: url_user.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                console.log(data);
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-usuario")[0].reset();
                alert_rspt_success(data);
                $('#modal-user').modal('hide');
            }

        }
    });
}
