var url_user = 'app/controller/PersonaController.php?ACTN=';

function initViewPeople() {
    tbUsuario(); 
    select_edo_civil();
    select_manzana();
    $("#form-usuario").on('submit', function (e) {
        addPersona(e);
    });
}

/**
 * ! Función addUser
 *   Se encarga de limpiar los campos del formulario
 *   de registro, porteriormente se muestra el modal.
 */
function addUser() {
    cleanInputs();
    $('#modal-user').modal({
        show: true,
        backdrop: 'static'
    });
}

function cleanInputs() {
    $("#txt_idpersona").val("");
    $("#txt_user").val("");
    $("#txt_apepat").val("");
    $("#txt_apemat").val("");
    $("#txt_edocivil").val("")
    $("#txt_edocivil").selectpicker();
    $("#txt_idmanzana").val("");
    $("#txt_idmanzana_u").selectpicker();
    $("#txt_direccion").val("");
}

/**
 * ! *Función addPersona
 * Envia los datos y la petición de registro al controlador
 * @param object e Evento que se lanza.
 */
function addPersona(e) {
    e.preventDefault();
    var formData = new FormData($("#form-usuario")[0]);


    $.ajax({
        url: url_user.concat('ADD'),
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data == "FAIL") {
                console.log(data);
                alert_rspt_error("Ocurrio un problema, vuelve a intentarlo");
            } else {
                $("#form-usuario")[0].reset();
                alert_rspt_success(data);
                tbUsuario();
                $('#modal-user').modal('hide');
            }

        }
    });
}



function goEditPersonaId(id) {
    //$("#form-usuario")[0].reset();
    $.post(url_user.concat('ROWID'),
        { 'txt_idpersona': id }, function (resp) {
            d = JSON.parse(resp);
            $("#txt_idpersona").val(id);
            $("#txt_user").val(d.nombre_persona);
            $("#txt_apepat").val(d.ape_pat);
            $("#txt_apemat").val(d.ape_mat);
            $("#txt_edocivil").val(d.id_edo_civil)
            $("#txt_edocivil").selectpicker('refresh');
            $("#txt_idmanzana").val(d.id_manzana);
            $("#txt_idmanzana").selectpicker('refresh');
            $("#txt_direccion").val(d.direccion);
            $('#modal-user').modal({
                show: true,
                backdrop: 'static'
            });
        });
}
var tabla_user = null;
function tbUsuario() {

    $('#tb_persona').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            url: url_user.concat('LIST'),
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[1, "desc"]],
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }
    }).buttons().container().appendTo('#tb_persona_wrapper .col-md-6');

}


