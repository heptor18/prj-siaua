//*  URL del controlador para la vista de gastos
var url_acceso = 'app/controller/AccesoController.php?ACTN=';


function initViewAcceso(){
    tbAcceso();         
}

function openModalAcceso(){
    $('#modal-acceso').modal({
        show: true,
        backdrop: 'static'
    });
}

function goAccesoID(id) {
    //alert("Ver información del usuario : " + id).show();
}

function goEditAcceso(id) {
   
}

function tbAcceso() {
    $("#tb_acceso").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true,
        "ajax": {
            url: url_acceso.concat('LIST'),
            type: "GET",
            dataType: "JSON",
            error: function (e) {
                console.log(e.responseText);
            },
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "oLanguage": {
            "sProcessing": "Procesando...",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del (_START_ al _END_) de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Por favor espere - cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            }
        }

    });
}

