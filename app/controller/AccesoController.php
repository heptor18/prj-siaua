<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {

   
    # Clase modelo
    require_once( '../model/AccesoModel.php');
    # Instacia de la clase
    $acceso = new AccesoModel();
    switch( $_GET[ 'ACTN' ] ) { 
        case "LIST": #Listado de accesos
            $rspta = $acceso->getListAccesos();
            $i = 1;
            $data = array();
            while($reg = $rspta->fetch_object()) { 
                //echo json_encode($reg);
                
                $data[] = array(
                    "0" => $i++,
                    "1" => $reg->nombre_comite,
                    "2" => $reg->desc_comite,
                    "3" => $reg->sesion != 1 ? '<span class="badge bg-danger">inactivo</span>' : '<span class="badge bg-success">activo</span>',
                    "4" => $reg->nombre_ciclo,
                    "5" => '<a class="btn bg-warning btn-sm" href="#" onclick="goEditManzanaId('.$reg->id_comite.');" >
                                <i class="fas fa-pencil-alt"></i> Editar
                            </a>
                            <a class="btn bg-navy btn-sm" href="#" onclick="informeManzanaId('.$reg->id_comite.');" >
                                <i class="fas fa-file-alt mr-2"></i> Informe
                            </a>
                            <a class="btn bg-danger btn-sm" href="#" onclick="deleteManzana('.$reg->id_comite.');" >
                                <i class="fas fa-trash-alt"></i> Eliminar
                            </a>'
                );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>