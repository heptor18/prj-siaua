<?php 
# Validamos si existe el parametro ACTN enviado por la url
if ( isset( $_GET[ 'ACTN' ] ) ) {
    $nombre_manzana = isset($_POST['txt_nombre_manzana']) ? ($_POST['txt_nombre_manzana']) : "";
    $desc_manzana = isset($_POST['txt_descripcion_manzana']) ? ($_POST['txt_descripcion_manzana']) : "";
    $txt_idmanzana = isset($_POST['txt_idmanzana']) ? ($_POST['txt_idmanzana']) : "";
   
    # Clase modelo
    require_once( '../model/ManzanaModel.php');
    # Instacia de la clase
    $manzana = new ManzanaModel();
    switch( $_GET[ 'ACTN' ] ) { 
        case "ADD":
            if(empty($txt_idmanzana)){ # Registra
                $rspta = $manzana->addManzana($nombre_manzana,$desc_manzana);
                echo $rspta ? "Registro exitoso" : "FAIL"; 
            } else { # Edita
                $rspta = $manzana->editManzana($txt_idmanzana, $nombre_manzana,$desc_manzana);
                echo $rspta ? "Edición exitosa" : "FAIL"; 
            }
            break;
        case "LIST": #Listado de manzanas
            $rspta = $manzana->getListManzana();
            $i = 1;
            $data = array();
            while($reg = $rspta->fetch_object()) { 
                //echo json_encode($reg);
                
                $data[] = array(
                    "0" => $i++,
                    "1" => $reg->nombre_manzana,
                    "2" => $reg->total_personas != 0 ? $reg->total_personas .' - Personas' : ' ---- ',
                    "3" => '<a class="btn bg-warning btn-sm" href="#" onclick="goEditManzanaId('.$reg->id_manzana.');" >
                                <i class="fas fa-pencil-alt"></i> Editar
                            </a>
                            <a class="btn bg-navy btn-sm" href="#" onclick="informeManzanaId('.$reg->id_manzana.');" >
                                <i class="fas fa-file-alt mr-2"></i> Informe
                            </a>
                            <a class="btn bg-danger btn-sm'. ($reg->total_personas != 0 ? ' disabled' : '').'" href="#" onclick="deleteManzana('.$reg->id_manzana.');" >
                                <i class="fas fa-trash-alt"></i> Eliminar
                            </a>'
                                    );
            }
            $res = array(
                "sEcho" => 1,
                "iTotalRecors" =>count($data),
                "iTotalDisplayRecords"=>count($data),
                "aaData"=>$data );
            echo json_encode($res);
             break;
        case "ROWID": # Obtiene fila correspondiente al id recibido
            $rspta = $manzana->getManzanaId($txt_idmanzana);
            echo $rspta ? json_encode($rspta) : "FAILD";
            break;
        case "SELECT_LIST":
            $rspta = $manzana->getSelect();
            echo '<option selected="selected" >SELECCIONA</option>';
			while($row = $rspta->fetch_object()){
				echo '<option value='.$row->id_manzana.'>'.$row->nombre_manzana.'</option>';
			}
            
            break;
        case "DETAILS":
            $rspta = $manzana->getDetailsManzanaId($txt_idmanzana);
            echo $rspta ? json_encode($rspta) : "FAILD";
            
            break;
        case "DELETEID":
            $rspta = $manzana->deleteManzanaID($txt_idmanzana);
            echo $rspta ? "Registro Eliminado ".$txt_idmanzana : "FAILD ".$txt_idmanzana;
            break;
        default:
            echo "Ocurrio un error intentelo mas tarde";
            break;
    }
} else{
    header("Laocation:../app/view/page/ErrorRuta.php");
}
?>