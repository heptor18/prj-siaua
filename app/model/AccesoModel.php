<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * AccesoModel
 */
class AccesoModel{
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){

    }
    
    /**
     * Method getListAccesos
     *
     * @return Object
     */
    public function getListAccesos(){
        return queryExecute("SELECT 
                                CONCAT(p.nombre_persona,' ',p.ape_pat) AS nombre_comite,
                                c.id_comite,
                                c.desc_comite,
                                c.sesion,
                                cl.nombre_ciclo,
                                cl.ciclo_actual
                            FROM 
                                comite c INNER JOIN persona p 
                            ON 
                                c.id_persona = p.id_persona 
                            INNER JOIN 
                                ciclo cl ON
                                c.id_ciclo = cl.id_ciclo");
    }

    

}