<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * ManzanaModel
 */
class ManzanaModel{
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){

    }
    
    /**
     * Method getListManzana
     *  Obtiene el listado de Manzanas con el numero de personas que pertenecen a esta.
     * @return Object
     */
    public function getListManzana(){
        return queryExecute("SELECT m.*,
                             (SELECT COUNT(id_persona) FROM persona WHERE id_manzana=m.id_manzana) AS total_personas 
                            FROM manzana m");
    }
    
    /**
     * Method getManzanaId
     *  Obtiene el registro segun el ID que se espesifica como parametro
     * @param int $id Identificafdor de Fila
     *
     * @return array
     */
    public function getManzanaId($id){
        return queryRowID("SELECT * FROM manzana WHERE id_manzana=$id");
    }
    
    /**
     * Method addManzana
     *  Registra una nueva Manzana
     * @param string $nombre_manzana Nombre de la manzana
     * @param string $des_manzana Descripción de manzana
     *
     * @return boolean
     */
    public function addManzana($nombre_manzana, $des_manzana){
        return queryExecute("INSERT INTO manzana VALUES(0,'$nombre_manzana','$des_manzana')");
    }
        
    /**
     * Method editManzana
     *  Edita los datos de un registro
     * @param int $id_manzana Identificador del registro
     * @param string $nombre_manzana Nombre de la manzana
     * @param string $des_manzana Descripción de manzana
     *
     * @return boolean
     */
    public function editManzana($id_manzana, $nombre_manzana, $des_manzana){
        return queryExecute("UPDATE manzana SET nombre_manzana ='$nombre_manzana',
                                                des_manzana='$des_manzana'
                                          WHERE id_manzana=$id_manzana");
    }
    
    /**
     * Method getSelect
     *  Llena elemento SELECT con los datos de las manzanas registradasa
     * @return Object
     */
    public function getSelect(){
        return queryExecute("SELECT * FROM manzana");
    }

    
    /**
     * Method getDetailsManzanaId
     *
     * @param int $id Identificadr del registro que se requiere obtener más informción
     *
     * @return array
     */
    public function getDetailsManzanaId($id){
        return queryRowID("SELECT m.*,
                                (SELECT COUNT(*) FROM persona WHERE id_manzana = m.id_manzana) AS numero_personas,
                                (SELECT COUNT(*) FROM servicio WHERE id_manzana = m.id_manzana AND estado_servicio = 1) AS numero_serv_activos,
                                (SELECT COUNT(*) FROM servicio WHERE id_manzana = m.id_manzana AND estado_servicio = 0) AS numero_serv_inactivos
                            FROM
                                manzana m
                            WHERE 
                                m.id_manzana=$id");
    }
    
    /**
     * Method deleteManzanaID
     *
     * @param int $id Identificadr del registro que se requiere eliminar
     *
     * @return boolean
     */
    public function deleteManzanaID($id){  
        return queryExecute("DELETE FROM manzana WHERE id_manzana=$id");
    }

}
?>