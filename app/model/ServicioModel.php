<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * ServicioModel
 */
class ServicioModel{
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){   }
    
    /**
     * Method getListServicio
     * Obtiene TODOS los registros de la tabla servicios
     * @return Object
     */
    public function getListServicio(){ 
        return queryExecute("SELECT 
                                p.id_persona,
                                CONCAT(p.nombre_persona,' ',p.ape_pat) AS responsable, 
                                m.nombre_manzana, m.id_manzana,
                                (SELECT COUNT(*) FROM servicio WHERE id_persona = p.id_persona) AS servicios_persona
                            FROM 
                                persona p INNER JOIN manzana m
                            ON 
                                p.id_manzana = m.id_manzana" 
                            );
    }
    
    /**
     * Method getServicioId
     * Obtiene el registro que coincida con el $id
     * @param int $id Identificador de servicio
     * @return array
     */
    public function getServicioId($id){ 
        return queryRowID("SELECT * FROM servicio WHERE id_servicio=$id");
    }
    
    /**
     * Method addServicio
     * Registra un nuevo servicio
     * @param string $direccion Dirección del servicio
     * @param string $desc_servicio Descripción para identificar el servicio
     * @param string $fecha Fecha en que se registro el servicio
     * @param boolean $estado Activo o Inactivo
     * @param int $id_persona Identificador de la persona que le pertenece el servicio
     * @param int $id_manzana Identificador de la Manzana en donde se ubica el servicio
     *
     * @return boolean
     */
    public function addServicio($direccion, $desc_servicio, $fecha, $id_persona, $id_manzana){
        return queryExecute("INSERT INTO servicio
                                VALUES(0, '$direccion', '$desc_servicio', '$fecha',1, $id_persona, $id_manzana)");
    }    
    /**
     * Method editServicio
     *
     * @param int $id Identificador del servicio
     * @param string $direccion Dirección del servicio
     * @param string $desc_servicio Descripción para identificar el servicio
     * @param string $fecha Fecha en que se registro el servicio
     * @param boolean $estado Activo o Inactivo
     * @param int $id_persona Identificador de la persona que le pertenece el servicio
     * @param int $id_manzana Identificador de la Manzana en donde se ubica el servicio
     * 
     * @return boolean
     */
    public function editServicio($id, $direccion, $desc_servicio, $fecha, $id_persona, $id_manzana){
        return queryExecute("UPDATE servicio SET direccion='$direccion',
                                                desc_servicio='$desc_servicio',
                                                fecha_registro='$fecha',
                                                id_persona=$id_persona,
                                                id_manzana=$id_manzana
                                            WHERE id_servicio=$id");      
    }
}
?>