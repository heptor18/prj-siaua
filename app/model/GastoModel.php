<?php 
# Se inclulle la conexión a la DB.
require_once("../config/Conexion.php"); 

/**
 * GastoModel
 */
class GastoModel{
        
    /**
     * Method __construct
     *
     * @return void
     */
    public function __construct(){   }
    
    /**
     * Method getListGasto
     * - Obtiene el listado de todos los gastos registrados
     * @return object
     */
    public function getListGasto(){
        return queryExecute("SELECT 
                            CONCAT(p.nombre_persona,' ',p.ape_pat) AS nombre_comite,
                            g.id_gasto,
                            g.fecha_gasto,
                            g.monto_gasto,
                            g.desc_gasto,
                            g.img_nota,
                            c.desc_comite
                        FROM 
                            persona p INNER JOIN comite c
                        ON 
                            p.id_persona = c.id_persona
                        INNER JOIN
                            gasto g 
                        ON
                            g.id_comite = c.id_comite
                        ");
    }
    
    
    /**
     * Method addGasto
     *  - Realiza el registro de un nuevo gasto.
     * @param String $fecha Fecha en que se realiza el gasto
     * @param Double $monto Cantidad que se pago
     * @param String $desc Descripción sobre lo que se compro
     * @param String $img Imagen de factura o nota si es que existiera
     * @param Int $id_comite Identificador del comite que realizo el gasto
     *
     * @return Boolean
     */
    public function addGasto($fecha,$monto,$desc,$img='',$id_comite){
        return queryExecute("INSERT INTO gasto
                            VALUES(
                                0,
                                '$fecha',
                                 $monto,
                                '$desc',
                                '$img',
                                 $id_comite
                            )");
    }
    
    /**
     * Method editGasto
     * - Modifica la informaión se un gasto ya registrado
     * @param Int $id Identificador del registro a modificar
     * @param String $fecha Fecha en que se realiza el gasto
     * @param Double $monto Cantidad que se pago
     * @param String $desc Descripción sobre lo que se compro
     * @param String $img Imagen de factura o nota si es que existiera
     * @param Int $id_comite Identificador del comite que realizo el gasto
     *
     * @return Boolean
     */
    public function editGasto($id,$fecha,$monto,$desc,$img='',$id_comite){
        return queryExecute("UPDATE gasto SET 
                                    fecha_gasto='$fecha',
                                    monto_gasto=$monto,
                                    desc_gato='$desc',
                                    img_nota='$img',
                                    id_comite=$id_comite 
                                WHERE
                                    id_gasto=$id");
    }


    public function getGastoID($id){
        return queryRowID("SELECT * FROM gasto WHERE id_gasto=$id");
    }

    /**
     * Method getListGastoFecha
     *
     * @param $fecha $fecha [explicite description]
     *
     * @return void
     */
    public function getGastoRangoFecha($fecha_ini, $fecha_end){}
    
    /**
     * Method deleteGasto
     *  Elimina un registro
     * @param Int $id Identificador del regsitro a eliminar
     *
     * @return Boolean
     */
    public function deleteGasto($id){
        return queryExecute("DELETE FROM gasto WHERE id_gasto=$id");
    }
}