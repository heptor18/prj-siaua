<?php
    require_once 'configuracion.php';
 
    $con = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    mysqli_query($con, 'SET NAMES "' . DB_ENCODE . '"');
    if (mysqli_connect_errno()) {
        printf("Fallo conexion %s\n", mysqli_connect_error());
        exit();
    }
    

    
    if (!function_exists('queryExecute')){
        # Funcion que retorna 1 -> OK, 0 -> NO.
        function queryExecute($sql){
            global $con;
            $recuest=$con->query($sql);
            return $recuest;
        }
        # Devuelve un solo registro
        function queryRowID($sql){
            global $con;
            $query=$con->query($sql);
            $row =$query->fetch_assoc();
            return $row;
        }
        function charSet($str){
             global $con;
             $str = mysqli_real_escape_string($con,trim($str));
             return htmlspecialchars($str);
        }
        
        /**
         * Method getUitimoID
         * Obtine el ultimo id que se registro
         * @param String $sql Consulta insert
         *
         * @return int Ultimo id que se inserto en la tabla
         */
        function getUitimoID($sql){
            global $con;
            $query = $con->query($sql);
            return $con->insert_id;
        }

    }
