-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-08-2022 a las 02:52:09
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `siaua`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--

CREATE TABLE `ciclo` (
  `id_ciclo` int(11) NOT NULL,
  `nombre_ciclo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `ciclo_actual` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ciclo`
--

INSERT INTO `ciclo` (`id_ciclo`, `nombre_ciclo`, `fecha_inicio`, `fecha_fin`, `ciclo_actual`) VALUES
(1, 'Periodo 2022 al 2023', '2022-08-26', '2022-09-09', 1),
(2, 'Periodo 2022 al 2023', '2022-01-01', '2023-01-01', 0),
(3, 'Enero del 2021', '2000-01-01', '2004-02-12', 0),
(4, 'Periodo 2022 al 2023', '2022-08-10', '2022-08-11', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comite`
--

CREATE TABLE `comite` (
  `id_comite` int(11) NOT NULL,
  `user` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `desc_comite` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `sesion` tinyint(4) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_ciclo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comite`
--

INSERT INTO `comite` (`id_comite`, `user`, `pass`, `desc_comite`, `fecha_ingreso`, `sesion`, `id_persona`, `id_ciclo`) VALUES
(1, 'hhector', 'hhector', 'Comite - Agua potable', '2022-08-13', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edo_civil`
--

CREATE TABLE `edo_civil` (
  `id_edo_civil` int(11) NOT NULL,
  `estado_civil` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `edo_civil`
--

INSERT INTO `edo_civil` (`id_edo_civil`, `estado_civil`) VALUES
(1, 'Saltero(a)'),
(2, 'Casado(a)'),
(3, 'Viudo(a)'),
(4, 'Divorciado(a)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gasto`
--

CREATE TABLE `gasto` (
  `id_gasto` int(11) NOT NULL,
  `fecha_gasto` date NOT NULL,
  `monto_gasto` double NOT NULL,
  `desc_gasto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `img_nota` blob DEFAULT NULL,
  `id_comite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gasto`
--

INSERT INTO `gasto` (`id_gasto`, `fecha_gasto`, `monto_gasto`, `desc_gasto`, `img_nota`, `id_comite`) VALUES
(1, '2022-08-13', 520, 'Energia Electrica', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manzana`
--

CREATE TABLE `manzana` (
  `id_manzana` int(11) NOT NULL,
  `nombre_manzana` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `des_manzana` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `manzana`
--

INSERT INTO `manzana` (`id_manzana`, `nombre_manzana`, `des_manzana`) VALUES
(1, 'LOS FRESNOS', 'MANZANA LOS FRESNOS'),
(2, 'LA PLAYA', 'MANZANA LA PLAYA'),
(3, 'LA PALMA', 'MANZANA LA PALMA'),
(4, 'LA FORNTERA', 'MANZANA LA FORNTERA'),
(5, 'CENTRO', 'MANZANA CENTRO'),
(6, 'BARRIO ALTO', 'MANZANA BARRIO ALTO'),
(7, 'FRACCIONAMIENTO', 'MANZANA FRACCIONAMIENTO'),
(8, '3 DE MAYO', 'MANZANA 3 DE MAYO'),
(9, 'Nueva', 'Nueva Manzana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mes`
--

CREATE TABLE `mes` (
  `id_mes` int(11) NOT NULL,
  `nombre_mes` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_clave` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `id_recibo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `mes`
--

INSERT INTO `mes` (`id_mes`, `nombre_mes`, `nombre_clave`, `id_recibo`) VALUES
(1, 'Enero', 'ENE', 1),
(2, 'Febrero', 'FEB', 1),
(3, 'Marzo', 'MARZ', 1),
(4, 'Mayo', 'MAY', 1),
(5, 'Junio', 'JUN', 1),
(6, 'Julio', 'JUL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL,
  `nombre_persona` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `ape_pat` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `ape_mat` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `estado_persona` tinyint(4) NOT NULL,
  `id_manzana` int(11) NOT NULL,
  `id_edo_civil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `nombre_persona`, `ape_pat`, `ape_mat`, `direccion`, `estado_persona`, `id_manzana`, `id_edo_civil`) VALUES
(1, 'Hector', 'Hernandez', 'Estrada', 'Av Reforma S/N, San Gabriel, Tezontepec de Aldama Hgo.', 1, 8, 1),
(2, 'Gladis', 'Falcón', 'Amado', 'Av Tula S/N, Atengo, Tezontepec de Aldama Hgo.', 1, 3, 2),
(3, 'EVA', 'JIMENEZ', 'ACEVEDO', 'CALLE LOS FRESNOS', 1, 3, 1),
(4, 'SAUL', 'ALVAREZ', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 4, 2),
(5, 'DELIA', 'CERON', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 8, 1),
(6, 'RAFAEL', 'CERON', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 5, 3),
(7, 'FLORENCIO', 'CERON', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 6, 1),
(8, 'EVER ZAID', 'HERNANDEZ', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 7, 2),
(9, 'Hector', 'Falcon', 'AGUILAR', 'Av. Atengo Texontepec de Aldama Hidalgo', 1, 6, 4),
(10, 'EDUARDO', 'JIMENEZ', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 2, 1),
(11, 'ARMANDO', 'RAMIREZ', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 3, 1),
(12, 'FLAVIO', 'RAMIREZ', 'AGUILAR', 'CALLE LOS FRESNOS', 1, 3, 3),
(13, 'ANA GABRIELA', 'BARRERA', 'ALVAREZ', 'CALLE LOS FRESNOS', 1, 4, 1),
(14, 'JUAN MANUEL', 'BARRERA', 'ALVAREZ', 'CALLE LOS FRESNOS', 1, 8, 4),
(15, 'ERASMO', 'GARCIA', 'ALVAREZ', 'CALLE LOS FRESNOS', 1, 5, 1),
(16, 'MARIA', 'SOLIS', 'ANGELES', 'CALLE LOS FRESNOS', 1, 6, 4),
(17, 'LEYLANY', 'MEZA', 'ANGELES', 'CALLE LOS FRESNOS', 1, 7, 1),
(18, 'Juan', 'Valdez', 'Hernández', 'Calle 5 de Mayo', 1, 2, 3),
(19, 'SILVESTRE', 'MONTERROSA', 'BALTAZAR', 'CALLE LOS FRESNOS', 1, 2, 1),
(20, 'MARIA DE LA CRUZ', 'RAMIREZ', 'BELLO', 'CALLE LOS FRESNOS', 1, 8, 2),
(21, 'Maria de Jesus', 'Gonzalez ', 'Angeles', 'Av Reforma S/N', 1, 7, 2),
(22, 'Alberta', 'Estrada', 'Castillo', 'Calle Moderna S/N', 1, 1, 2),
(23, 'Emilio', 'Hernández', 'Gonzaléz', 'Av. Reforma S/N', 1, 1, 2),
(24, 'Santiago', 'Flores', 'Falcón', 'Calle Hidalgo S/N', 1, 7, 4),
(25, 'Jose', 'Garcia', 'Luna', 'Av Insurgentes', 1, 6, 4),
(26, 'Doroteo', 'Angeles', 'Perez', 'Calle Melchor Ocampo', 1, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibo`
--

CREATE TABLE `recibo` (
  `id_recibo` int(11) NOT NULL,
  `no_folio` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_pago` date NOT NULL,
  `concepto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `monto` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recibo`
--

INSERT INTO `recibo` (`id_recibo`, `no_folio`, `fecha_pago`, `concepto`, `monto`) VALUES
(1, '00001', '2022-08-13', 'Servicio de agua', 360);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `desc_servicio` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_registro` date NOT NULL,
  `estado_servicio` tinyint(4) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_manzana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `direccion`, `desc_servicio`, `fecha_registro`, `estado_servicio`, `id_persona`, `id_manzana`) VALUES
(1, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 9, 3),
(2, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 11, 3),
(3, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 2, 3),
(4, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 2, 3),
(5, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 25, 3),
(6, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(7, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(8, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(9, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 18, 3),
(10, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 25, 3),
(11, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 7, 3),
(12, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(13, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 13, 3),
(14, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(15, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(16, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 18, 3),
(17, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 20, 3),
(18, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(19, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(20, 'Miguel Hidago', 'HHHHHHHHHH', '2022-08-22', 1, 1, 3),
(21, 'Colonia Centro', 'Local del centro', '2022-08-22', 1, 16, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE `tarjeta` (
  `id_tarjeta` int(11) NOT NULL,
  `numero_tarjeta` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `estado_tarjeta` tinyint(4) NOT NULL,
  `id_recibo` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `id_ciclo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarjeta`
--

INSERT INTO `tarjeta` (`id_tarjeta`, `numero_tarjeta`, `estado_tarjeta`, `id_recibo`, `id_servicio`, `id_ciclo`) VALUES
(1, '02021', 0, 1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  ADD PRIMARY KEY (`id_ciclo`);

--
-- Indices de la tabla `comite`
--
ALTER TABLE `comite`
  ADD PRIMARY KEY (`id_comite`),
  ADD KEY `ciclo_comite_fk` (`id_ciclo`),
  ADD KEY `persona_comite_fk` (`id_persona`);

--
-- Indices de la tabla `edo_civil`
--
ALTER TABLE `edo_civil`
  ADD PRIMARY KEY (`id_edo_civil`);

--
-- Indices de la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD PRIMARY KEY (`id_gasto`),
  ADD KEY `comite_gasto_fk` (`id_comite`);

--
-- Indices de la tabla `manzana`
--
ALTER TABLE `manzana`
  ADD PRIMARY KEY (`id_manzana`);

--
-- Indices de la tabla `mes`
--
ALTER TABLE `mes`
  ADD PRIMARY KEY (`id_mes`),
  ADD KEY `recibo_mes_fk` (`id_recibo`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`),
  ADD KEY `edo_civil_persona_fk` (`id_edo_civil`),
  ADD KEY `manzana_persona_fk` (`id_manzana`);

--
-- Indices de la tabla `recibo`
--
ALTER TABLE `recibo`
  ADD PRIMARY KEY (`id_recibo`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`),
  ADD KEY `manzana_servicio_fk` (`id_manzana`),
  ADD KEY `persona_servicio_fk` (`id_persona`);

--
-- Indices de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD PRIMARY KEY (`id_tarjeta`),
  ADD KEY `ciclo_tarjeta_fk` (`id_ciclo`),
  ADD KEY `recibo_tarjeta_fk` (`id_recibo`),
  ADD KEY `servicio_tarjeta_fk` (`id_servicio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciclo`
--
ALTER TABLE `ciclo`
  MODIFY `id_ciclo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `comite`
--
ALTER TABLE `comite`
  MODIFY `id_comite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `edo_civil`
--
ALTER TABLE `edo_civil`
  MODIFY `id_edo_civil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `gasto`
--
ALTER TABLE `gasto`
  MODIFY `id_gasto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `manzana`
--
ALTER TABLE `manzana`
  MODIFY `id_manzana` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `mes`
--
ALTER TABLE `mes`
  MODIFY `id_mes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `recibo`
--
ALTER TABLE `recibo`
  MODIFY `id_recibo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  MODIFY `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comite`
--
ALTER TABLE `comite`
  ADD CONSTRAINT `ciclo_comite_fk` FOREIGN KEY (`id_ciclo`) REFERENCES `ciclo` (`id_ciclo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `persona_comite_fk` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `gasto`
--
ALTER TABLE `gasto`
  ADD CONSTRAINT `comite_gasto_fk` FOREIGN KEY (`id_comite`) REFERENCES `comite` (`id_comite`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mes`
--
ALTER TABLE `mes`
  ADD CONSTRAINT `recibo_mes_fk` FOREIGN KEY (`id_recibo`) REFERENCES `recibo` (`id_recibo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `edo_civil_persona_fk` FOREIGN KEY (`id_edo_civil`) REFERENCES `edo_civil` (`id_edo_civil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `manzana_persona_fk` FOREIGN KEY (`id_manzana`) REFERENCES `manzana` (`id_manzana`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD CONSTRAINT `manzana_servicio_fk` FOREIGN KEY (`id_manzana`) REFERENCES `manzana` (`id_manzana`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `persona_servicio_fk` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD CONSTRAINT `ciclo_tarjeta_fk` FOREIGN KEY (`id_ciclo`) REFERENCES `ciclo` (`id_ciclo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `recibo_tarjeta_fk` FOREIGN KEY (`id_recibo`) REFERENCES `recibo` (`id_recibo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `servicio_tarjeta_fk` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
